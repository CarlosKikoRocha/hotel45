package org.queuecumbers.hotel45.persistence.model;

public abstract class AbstractModel {

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
